node-keypress (0.2.1-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Change priority extra to priority optional.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers to use salsa repository.
  * Remove unnecessary get-orig-source-target.
  * Apply multi-arch hints.
    + node-keypress: Add Multi-Arch: foreign.
  * Remove constraints unnecessary since buster:
    + node-keypress: Drop versioned constraint on nodejs in Depends.
  * Bump debhelper from old 12 to 13.

  [ Yadd ]
  * Declare compliance with policy 4.6.0
  * Add "Rules-Requires-Root: no"
  * Change section to javascript
  * Add debian/gbp.conf
  * Modernize debian/watch
    * Fix filenamemangle
    * Fix GitHub tags regex
  * Drop nodejs dependency
  * Use dh-sequence-nodejs auto install

 -- Yadd <yadd@debian.org>  Fri, 12 Nov 2021 20:16:35 +0100

node-keypress (0.2.1-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright:
    + Use Expat instead of MIT as license keyword.
  * debian/control:
    + Bump Standards: to 3.9.5. No changes needed.
    + Alioth-canonicalize Vcs-*: fields.
    + Move packaging Git to pkg-javascript namespace on Alioth.
  * debian/rules:
    + Add get-orig-source rule.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 20 Aug 2014 13:39:19 +0200

node-keypress (0.1.0-2) unstable; urgency=low

  * /debian/control:
    + Fix versioned dependency on nodejs.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 15 May 2013 11:14:32 +0200

node-keypress (0.1.0-1) unstable; urgency=low

  * Initial release. (Closes: #707556).

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 09 May 2013 14:06:51 +0200
